const Company = require("../models/company");
const User = require("../models/user");

const { validationResult } = require("express-validator");

exports.getCreatecompany = async (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

  if (!errors.isEmpty()) {
    const error = new Error("validation faild.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }
  const { name, url, location, description } = req.body;
  const _company = new Company({
    name,
    url,
    location,
    description,
  });
  try {
    const result = await _company.save();
    res.status(201).json({
      message: "Comapny Created",
      company: result,
    });
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getUpdatecompany = async (req, res, next) => {
  const compId = req.params.companyId;
  const { name, url, location, description } = req.body;
  try {
    const _company = await Company.findById(compId);
    _company.name = name;
    _company.url = url;
    _company.location = location;
    _company.description = description;
    const result = await _company.save();
    res.status(201).json({
      message: "Company Updated",
      company: result,
    });
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getcompanies = async (req, res, next) => {
  try {
    const companies = await Company.find({});
    res.status(200).json({
      success: "true",
      companies,
    });
  } catch (error) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.postDeletecompany = async (req, res, next) => {
  const compId = req.params.companyId;
  try {
    const result = await Company.findByIdAndRemove(compId);

    res.sttus(200).json({
      success: "true",
      message: "Company deleted Successfully",
    });
  } catch (error) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.postCreateUser = async (req, res) => {
  try {
    const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {
      return res.send({
        status: "false",
        error: errors.array(),
      });
    }
    const { firstName, lastName, email, phone } = req.body;

    const user = await User.create({
      firstName,
      lastName,
      email,
      phone,
    });

    res.status(201).json({
      status: "true",
      message: "user created Succesfully",
      user: user,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getUpdateUser = async (req, res, next) => {
  const id = req.params.userId;
  const { firstName, lastName, email, phone } = req.body;
  if (firstName && phone && email && id) {
    try {
      const result = await User.update(
        { firstName, lastName, email, phone },
        { where: { id } }
      );
      res.status(200).json({
        status: "true",
        message: result,
      });
    } catch (err) {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    }
  } else {
    throw new Error("first name is required");
  }
};
exports.getAllUsers = async (req, res, next) => {
  const result = await User.findAll();

  res.status(200).json({
    success: "true",
    users: result,
  });
};
exports.postDeleteUser = async (req, res, next) => {
  const id = req.params.userId;
  console.log(id);
  try {
    await User.destroy({
      where: {
        id,
      },
    });
    return res.status(200).json({
      success: "true",
      message: "Deleted successfully",
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

// validation
const { body } = require("express-validator/check");

exports.validate = (method) => {
  switch (method) {
    case "postCreateUser": {
      return [
        body("firstName", "userName doesn't exists").exists(),
        body("email", "Invalid email").exists().isEmail(),
        body("phone").exists().isInt(),
      ];
    }
    case "getCreatecompany": {
      return [
        body("name", "userName doesn't exists").exists(),
        body("url", "Url doesn't exist").exists(),
        body("location", "location doesn't exists").exists(),
      ];
    }
  }
};
