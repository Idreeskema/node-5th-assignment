const User = require("../models/user");

 
exports.uploadFile = async (req, res, next) => {
  if (!req.file) {
    const error = new Error("No image provided.");
    error.statusCode = 422;
    throw error;
  }
  const _imageUrl = req.file.path;
  const _userId = req.body.userId;
  try {
    const updatedUser = await User.findOneAndUpdate(
      {
        _id: _userId,
      },
      { imageUrl: _imageUrl },
      { new: true }
    );
    return res.json({ message: "file uploaded successfully" });
  } catch (error) {
    return res.json({ error });
  }
};
