const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const sendgridTransport = require("nodemailer-sendgrid-transport");

const User = require("../models/user");
const io = require("../socket");
/* const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key:
        "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",
    },
  })
);
 */
const transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "2822d30ff629c7",
    pass: "45a4e8060c6c0e",
  },
});
exports.getLogin = (req, res, next) => {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render("auth/login", {
    path: "/login",
    pageTitle: "Login",
    errorMessage: message,
  });
};
exports.getSignup = (req, res, next) => {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render("auth/signup", {
    path: "/signup",
    pageTitle: "Signup",
    errorMessage: message,
  });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  User.findOne({ email: email })
    .then((user) => {
      if (!user) {
        req.flash("error", "Invalid email or password.");
        return res.redirect("/login");
      }
      bcrypt
        .compare(password, user.password)
        .then((doMatch) => {
          if (doMatch) {
            io.getIO().emit("login", {
              message: "Login Successfully",
            });
            req.session.isLoggedIn = true;
            req.session.user = user;
            /*     return req.session.save((err) => {
              console.log(err);
              
              res.redirect("/");
            }); */
            return res.status(201).json({
              status: "true",
              message: "user login Succesfully",
              user: user,
            });
          }
          req.flash("error", "Invalid email or password.");
          res.redirect("/login");
        })
        .catch((err) => {
          if (!err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
        });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.postSignup = async (req, res) => {
  try {
    /*     const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {
      return res.send({
        status: "false", 
        error: errors.array(),
      });
    } */
    const {
      firstName,
      lastName,
      email,
      phone,
      password,
      confirmPassword,
    } = req.body;

    if (password !== confirmPassword) {
      req.flash("error", "E-Mail exists already, please pick a different one.");
      return res.redirect("/signup");
    }
    _checkUser = await User.findOne({ email: email });
    if (_checkUser) {
      req.flash("error", "E-Mail exists already, please pick a different one.");
      return res.redirect("/signup");
    }

    const passwordHash = bcrypt.hashSync(password, 12);
    const _user = new User({
      firstName,
      lastName,
      email,
      phone,
      password: passwordHash,
    });
    const response = await _user.save();

    res.redirect("/login");
    return transport.sendMail({
      to: email,
      from: "talhapatel.tp@gmail.com",
      subject: "Signup succeeded!",
      html: "<h1>You successfully signed up!</h1>",
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
exports.postLogout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.redirect("/login");
  });
};
exports.loadSession = (req, res, next) => {
  return res.send(req.session.user);
};
exports.getUpdateUser = async (req, res, next) => {
  const id = req.params.userId;
  const { firstName, lastName, email, phone } = req.body;
  if (firstName && phone && email && id) {
    try {
      const _user = await User.findById(id);
      _user.firstName = firstName;
      _user.lastName = lastName;
      _user.email = email;
      _user.phone = phone;
      return res.send({
        status: "true",
        message: _user,
      });
    } catch (err) {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    }
  } else {
    throw Error("Validation Fail");
  }
};
exports.getAllUsers = async (req, res, next) => {
  try {
    const users = await User.find({});
    return res.send({
      success: "true",
      users,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
exports.postDeleteUser = async (req, res, next) => {
  const id = req.params.userId;

  try {
    const result = await User.findByIdAndRemove(id);

    res.status(200).json({
      success: "true",
      message: "Company deleted Successfully",
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
