const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const csrf = require("csurf");
const flash = require("connect-flash");
const dbURI = `mongodb+srv://root:12345@cluster0.w93q1.mongodb.net/job-portal?retryWrites=true&w=majority`;
const app = express();
const store = new MongoDBStore({
  uri: dbURI,
  collection: "sessions",
});

const csrfProtection = csrf();
const PORT = 8080;

//

app.use("/uploads", express.static(path.join(__dirname, "uploads")));
app.set("view engine", "ejs");
app.set("views", "views");

//const sequelize = require("./util/database");
const adminRoutes = require("./routes/admin");
const signUpRoutes = require("./routes/auth");
const fileUploadRoutes = require("./routes/fileUpload");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));
app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);
app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
  res.locals.isAuthenticated = req.session.isLoggedIn;
  res.locals.csrfToken = req.csrfToken();
  next();
});
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});
app.use("/admin", adminRoutes);
app.use(signUpRoutes);
app.use(fileUploadRoutes);
app.get("/about-us", (req, res, next) => {
  res.render("about-us", {
    pageTitle: "About Us",
    path: "about-us",
  });
});
app.get("/", (req, res, next) => {
  res.render("index", {
    pageTitle: "Welcome",
    path: "/",
  });
});

app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ message: message, data: data });
});
(async () => {
  try {
    await mongoose.connect(dbURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    // await sequelize.sync();

    const server = app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
    const io = require("./socket").init(server);
    io.on("connection", (socket) => {
      console.log("Client connected");
    });
  } catch (err) {
    console.log("error: " + err);
  }
})();
